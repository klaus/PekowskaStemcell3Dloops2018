inDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/fastq/"
outDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/aligned/"

for f in $( ls $inDir | grep .fastq.gz$ ); do      
    cmd="bowtie2 -p 7 /g/huber/projects/TCC/data/bowtie2-index/NCBIM37.67 -U "$inDir$f" -S"
    eval $cmd $outDir${f/.fastq.gz/.sam} \&
done
