inDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/filtered/"
outDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/unique/"

for f in $( ls $inDir | grep .bam$ ); do      
    cmd="samtools-0.1.19 rmdup -s "
    eval $cmd $inDir$f $outDir${f/.bam/_unique.bam} \&
done
