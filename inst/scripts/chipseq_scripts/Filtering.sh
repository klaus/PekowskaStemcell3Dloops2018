inDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/aligned/"
outDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/filtered/"

for f in $( ls $inDir | grep .sam$ ); do
	pipe='>'
    cmd="samtools view -bS -q 40 "
    eval $cmd $inDir$f $pipe $outDir${f/.sam/_filtered.bam} \&
done
