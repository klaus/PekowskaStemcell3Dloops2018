inDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/unique/replicates_merged/"
outDir="/g/huber/projects/TCC/Submission_2016/ChIP_seq_NIAMS/bed/"

for f in $( ls $inDir | grep .bam$ ); do      
    cmd="bedtools bamtobed -i "
	pipe=">"
    eval $cmd $inDir$f $pipe $outDir${f/.bam/.bed} \&
done
