\name{PekowskaStemcell3Dloops2018}
\docType{data}
\alias{PekowskaStemcell3Dloops2018}
\alias{PekowskaStemcell3Dloops2018-package}
\alias{ERRXXX}
\title{Data for the TCC study Pekowska et. al. 2018}
\description{
  The TCC and other data was processed
  according to the steps presented in \code{inst/scripts/make-data.R}
}
\usage{
ERRXXX(metadata=FALSE)
}
\arguments{
  \item{metadata}{
    \code{logical} value indicating whether metadata only should be returned
    or if the resource should be loaded. Default behavior(metadata=FALSE) 
    loads the data.
  }
}
\examples{
  ERRXXX()
}
\value{ TODO }
\format{TODO}
\source{TCC data TODO.}
\references{
  TODO
}
\keyword{datasets}
